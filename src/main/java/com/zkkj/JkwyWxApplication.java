package com.zkkj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement // 开启事务驱动
@ServletComponentScan
@MapperScan(basePackages = {"com.zkkj.modules.user.domain.mapper"})
public class JkwyWxApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(JkwyWxApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(JkwyWxApplication.class, args);
	}
}
