package com.zkkj.utils;

public class StringUtil {

	private static final char SEPARATOR = '_';

	/**
	 * @Description:带有下划线的数据库字段名转换成java中驼峰命名
	 * @param: @param columnName
	 * @param: @return
	 * @return: String
	 * @throws
	 */
	public static String toCamelName(String columnName) {

		if (columnName == null) {
			return null;
		}
		columnName = columnName.toLowerCase();
		StringBuilder sb = new StringBuilder(columnName.length());
		boolean upperCase = false;
		for (int i = 0; i < columnName.length(); i++) {
			char c = columnName.charAt(i);
			if (c == SEPARATOR) {
				upperCase = true;
			} else if (upperCase) {
				sb.append(Character.toUpperCase(c));
				upperCase = false;
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	/**
	 * @Description:java中驼峰命名转换成带有下划线的数据库字段名
	 * @param: @param s
	 * @param: @return
	 * @return: String
	 * @throws
	 */
	public static String toUnderlineName(String fieldName) {
		if (fieldName == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		boolean upperCase = false;
		for (int i = 0; i < fieldName.length(); i++) {
			char c = fieldName.charAt(i);
			boolean nextUpperCase = true;
			if (i < (fieldName.length() - 1)) {
				nextUpperCase = Character.isUpperCase(fieldName.charAt(i + 1));
			}
			if ((i >= 0) && Character.isUpperCase(c)) {
				if (!upperCase || !nextUpperCase) {
					if (i > 0)
						sb.append(SEPARATOR);
				}
				upperCase = true;
			} else {
				upperCase = false;
			}
			sb.append(Character.toLowerCase(c));
		}
		return sb.toString();

	}
}
