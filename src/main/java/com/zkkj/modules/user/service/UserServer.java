package com.zkkj.modules.user.service;

import com.zkkj.modules.user.domain.User;
import com.zkkj.modules.user.domain.mapper.UserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("userServer")
public class UserServer {
    Logger logger = LoggerFactory.getLogger(UserServer.class);

    @Autowired
    private UserMapper userMapper;

    public Integer addUser(User user) {
        return userMapper.addUser(user);
    }

    public Integer updateUser(User user) {
        return userMapper.updateUser(user);
    }

    public Integer deleteUser(User user) {
        return userMapper.deleteUser(user);
    }

    public User queryForUser(Integer userId) {
        return userMapper.queryForUser(userId);
    }

    public List<User> queryListForUser() {
        return userMapper.queryListForUser();
    }

    public List<Map<String, Object>> queryListForUserPage(Integer page, Integer pageSize) {
        return userMapper.queryListForUserPage(page, pageSize);
    }


}