package com.zkkj.modules.user.domain.mapper;

import org.apache.ibatis.jdbc.SQL;

public class UserDynamicSQLProvider {
    public String queryForUser(){
        return "select * from sys_users where id = #{id}";
    }

    public String queryForUserBySQL(){
        return new SQL() {
            {
                SELECT("*");
                FROM("sys_users");
                WHERE("id=#{id}");
            }
        }.toString();
    }

    public String  queryListForUserPage(Integer page, Integer pageSize) {
        String querySql = "SELECT * FROM sys_users LIMIT " + (page - 1) * pageSize +  "," + pageSize;
        return querySql;
    }

}
