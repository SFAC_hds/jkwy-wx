package com.zkkj.modules.user.domain.mapper;

import com.zkkj.modules.user.domain.User;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    public Integer addUser(User user);

    public Integer updateUser(User user);

    public Integer deleteUser(User user);

    // @Select("select * from sys_users where id = #{id}")
    @SelectProvider(type = UserDynamicSQLProvider.class, method = "queryForUserBySQL")
    public User queryForUser(Integer id);

    @Select("select * from sys_users")
    public List<User> queryListForUser();


    @SelectProvider(type = UserDynamicSQLProvider.class, method = "queryListForUserPage")
    public List<Map<String, Object>> queryListForUserPage(Integer page, Integer pageSize);

}