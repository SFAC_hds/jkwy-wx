package com.zkkj.modules.user.domain;

import java.util.Date;
import java.sql.Timestamp;
import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String account;
    private String password;
    private String name;
    private Integer sex;
    private String mobile;
    private Timestamp regTime;
    private String email;
    private Date birthday;
    private Integer enabled;
    private Integer wxId;
    private Integer employeeId;
    private String remark;
    private Integer isAdmin;
    private Integer orgId;
    private String payPassword;
    private String ppwQuestion;
    private String ppwAnswer;

    public Integer getId() {
        return id;
    }

    public Integer setId(Integer id) {
        return this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public String setAccount(String account) {
        return this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public String setPassword(String password) {
        return this.password = password;
    }

    public String getName() {
        return name;
    }

    public String setName(String name) {
        return this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public Integer setSex(Integer sex) {
        return this.sex = sex;
    }

    public String getMobile() {
        return mobile;
    }

    public String setMobile(String mobile) {
        return this.mobile = mobile;
    }

    public Timestamp getRegTime() {
        return regTime;
    }

    public Timestamp setRegTime(Timestamp regTime) {
        return this.regTime = regTime;
    }

    public String getEmail() {
        return email;
    }

    public String setEmail(String email) {
        return this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Date setBirthday(Date birthday) {
        return this.birthday = birthday;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public Integer setEnabled(Integer enabled) {
        return this.enabled = enabled;
    }

    public Integer getWxId() {
        return wxId;
    }

    public Integer setWxId(Integer wxId) {
        return this.wxId = wxId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public Integer setEmployeeId(Integer employeeId) {
        return this.employeeId = employeeId;
    }

    public String getRemark() {
        return remark;
    }

    public String setRemark(String remark) {
        return this.remark = remark;
    }

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public Integer setIsAdmin(Integer isAdmin) {
        return this.isAdmin = isAdmin;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public Integer setOrgId(Integer orgId) {
        return this.orgId = orgId;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public String setPayPassword(String payPassword) {
        return this.payPassword = payPassword;
    }

    public String getPpwQuestion() {
        return ppwQuestion;
    }

    public String setPpwQuestion(String ppwQuestion) {
        return this.ppwQuestion = ppwQuestion;
    }

    public String getPpwAnswer() {
        return ppwAnswer;
    }

    public String setPpwAnswer(String ppwAnswer) {
        return this.ppwAnswer = ppwAnswer;
    }
}