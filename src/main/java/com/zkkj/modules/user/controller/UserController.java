package com.zkkj.modules.user.controller;

import com.zkkj.modules.user.domain.User;
import com.zkkj.modules.user.service.UserServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserServer userServer;

    @ResponseBody
    @RequestMapping("/getUser")
    public User getUser (HttpServletRequest request, HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        User user = userServer.queryForUser(5853);
        return user;
    }

    @ResponseBody
    @RequestMapping("/queryUserList")
    public List<User> queryUserList() {
        return userServer.queryListForUser();
    }

    @ResponseBody
    @RequestMapping("/queryListForUserPage")
    public List<Map<String, Object>> queryListForUserPage(HttpServletRequest request, HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        String pageStr = request.getParameter("page");
        String pageSizeStr = request.getParameter("pageSize");
        Integer page = Integer.valueOf(pageStr);
        Integer pageSize = Integer.valueOf(pageSizeStr);
        return userServer.queryListForUserPage(page, pageSize);
    }
}
